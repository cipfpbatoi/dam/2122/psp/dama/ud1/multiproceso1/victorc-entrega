import java.util.Scanner;

public class Minusculas {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String minusculas = sc.nextLine();
        if (!minusculas.equals("finalizar")) {

            try {
                minusculas = minusculas.toLowerCase();
                System.out.println(minusculas);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
