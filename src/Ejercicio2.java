import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) {
        /**
         * Esta línea indica que ni se ha probado el programa
         */
        String command = "java, \\src\\Random10.java";
        ArrayList argsList = new ArrayList<>(Arrays.asList(command.split(",")));
        ProcessBuilder pb = new ProcessBuilder(argsList);

        try {
            Process process = pb.start();
            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);
            Scanner procesoSC = new Scanner(process.getInputStream());
            Scanner sc = new Scanner(System.in);
            String linea = sc.nextLine();

            while (!linea.equals("stop")) {

                bw.write(linea);
                bw.newLine();
                bw.flush();
                System.out.println(procesoSC.nextLine());
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
