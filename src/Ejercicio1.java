import java.io.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {
    public static void main(String[] args) {

        ProcessBuilder pb = new ProcessBuilder(args);

        try {
            Process process = pb.start();

            InputStream inputStream = process.getInputStream();

            Scanner scanner = new Scanner(inputStream);

            if (!process.waitFor(2, TimeUnit.SECONDS)) {
                System.out.println("el programa tarda en responder");
            } else {

                pb.inheritIO();

                /**
                 * Si se utiliza inheritIO no es necesario 
                 * utilizar el resto de cosas, de hecho, no debería de funcionar.
                 */
                while (scanner.hasNext()) {
                    System.out.println(scanner.nextLine());
                }
            }
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
